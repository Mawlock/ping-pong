#This is just a file that is used to quickly test out various functions by
#taking advantage of the fact that it is in the same folder.
#You can delete this file without any harm to the project. But don't do it:)

import sys, pygame
pygame.init()
size = width, height = 320, 240
speed = [0, 0]
black = 0, 0, 0

screen = pygame.display.set_mode(size)

ball = pygame.image.load("media\\ball.gif")
ballrect = ball.get_rect()
ballrect.center = (0,0)

font_name = pygame.font.get_default_font()
font_path = pygame.font.match_font(font_name)
font = pygame.font.Font(font_path, 50)

Text = font.render("Superman always wins",True,(255,255,255))

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    ballrect = ballrect.move(speed)
    if ballrect.left < 0 or ballrect.right > width:
        speed[0] = -speed[0]
    if ballrect.top < 0 or ballrect.bottom > height:
        speed[1] = -speed[1]

    screen.fill(black)
    screen.blit(ball, ballrect)
    screen.blit(Text, Text.get_rect())
    pygame.display.flip()
