#File has been edited
import sys
import pygame
import controllers
import update_display
import collision_detection

pygame.init()

screen_size = width, height = 800, 600

screen = pygame.display.set_mode(screen_size)

pygame.key.set_repeat(4, 4)

#Initialize some game variables
paddle_speed = 3
paddle1time = 0
paddle2time = 0
ball_speed = [1, 1]
font_name = pygame.font.get_default_font()
font_path = pygame.font.match_font(font_name)
font = pygame.font.Font(font_path, 50)

score = {"left": 0, "right": 0}

last_time = pygame.time.get_ticks()

ball_image = pygame.image.load("media\\ball.gif")
paddle_image = pygame.image.load("media\\paddle.gif")
paddle1rect = paddle_image.get_rect()
paddle2rect = paddle_image.get_rect()
ball_rect = ball_image.get_rect()
paddle_screenside_border = 20

#Position paddles into their proper positions
ball_rect.center = screen_size[0] / 2, screen_size[1] / 2
paddle1rect = paddle1rect.move([paddle_screenside_border, height / 2])
paddle2rect = paddle2rect.move([width - paddle_screenside_border - 20,
     height / 2])

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            paddle1rect, paddle2rect = controllers.check_controllers_status(
                event, paddle1rect, paddle2rect, paddle_speed)

    #Check for collisions and other occurences
    ball_speed, temp_ball_rect, score = collision_detection.check_for_collision(
        screen_size, score, ball_speed, ball_rect, paddle1rect, paddle2rect)

    #Moves the ball in whatever direction the balls speed is directing it to
    ball_rect = ball_rect.move([x * (pygame.time.get_ticks()
    - last_time) / 2 for x in ball_speed])

    last_time = pygame.time.get_ticks()  # Update time for animation

    #Check if the ball has gone out of bounds. If so, reset it to the center
    if temp_ball_rect != None:
        ball_rect = temp_ball_rect

    #Update the screen with all the work we did.
    update_display.update_display(screen, screen_size, font, score, ball_image,
         paddle_image, ball_rect, paddle1rect, paddle2rect)
