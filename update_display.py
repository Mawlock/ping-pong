import pygame


def update_display(screen, screen_size, font, score, ball_image, paddle_image, ball_rect,
    paddle1rect, paddle2rect):
    """Updates the display by drawing the images on a surface and flipping
    it onto the screen"""

    screen.fill([0, 0, 0])
    screen.blit(ball_image, ball_rect)
    screen.blit(paddle_image, paddle1rect)
    screen.blit(paddle_image, paddle2rect)

    left_score_text = "Left Score: " + str(score["left"])
    right_score_text = "Right Score: " + str(score["right"])
    left_score_display = font.render(left_score_text, True, (255, 255, 255))
    right_score_display = font.render(right_score_text, True, (255, 255, 255))

    left_score_rect = left_score_display.get_rect()
    right_score_rect = right_score_display.get_rect()
    left_score_rect.center = (screen_size[0] / 4), 20
    right_score_rect.center = (screen_size[0] - (screen_size[0] / 4)), 20

    screen.blit(left_score_display, left_score_rect)
    screen.blit(right_score_display, right_score_rect)

    pygame.display.flip()
